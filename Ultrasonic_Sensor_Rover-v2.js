let d1 = 0
let d2 = 0

basic.forever(function () {
    if (maqueen.sensor(PingUnit.Centimeters) < 15 && maqueen.sensor(PingUnit.Centimeters) != 0) {
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
        basic.pause(500)
        
        d1 = maqueen.sensor(PingUnit.Centimeters)
        
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
        basic.pause(1000)
        
        d2 = maqueen.sensor(PingUnit.Centimeters)
        
        if (d2 < d1) {
            maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
            maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
            basic.pause(1000)
        }
    } else {
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 75)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 75)
    }
})
