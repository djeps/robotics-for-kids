let distance = 0
let distance_meas = 0
let duration = 0
let samples = 0
let k1 = 0.0271
let k2 = 0.9222

basic.forever(function () {
    for (let i = 0; i < 10; i++) {
        samples += 1
        pins.digitalWritePin(DigitalPin.P1, 0)
        control.waitMicros(2)
        pins.digitalWritePin(DigitalPin.P1, 1)
        control.waitMicros(10)
        pins.digitalWritePin(DigitalPin.P1, 0)
        duration = pins.pulseIn(DigitalPin.P2, PulseValue.High)
        distance_meas = duration * k1
        distance_meas = distance_meas - k2
        distance = distance + distance_meas
    }
    
    distance = distance / samples
    basic.pause(100)
    basic.showNumber(Math.round(distance))
    basic.pause(100)
    samples = 0
    distance = 0
    distance_meas = 0
})

