let cnt = 0
let pos = 0
let max = 0
let d = 0
let deg = 0

function sweep () {
    pivot_l(60)
    maqueen.motorStopAll()
    
    cnt = 0
    pos = 0
    
    max = maqueen.sensor(PingUnit.Centimeters)
    
    for (let i = 0; i < 13; i++) {
        pivot_r(10)
        maqueen.motorStopAll()
        
        d = maqueen.sensor(PingUnit.Centimeters)
        
        cnt += 1
        if (d >= max) {
            max = d
            pos = cnt
        }
        
        basic.pause(10)
    }
    
    maqueen.motorStopAll()
    
    if (max >= 10) {
        deg = (12 - pos) * 10
        pivot_l(deg)
    } else {
        // Basically opposite of the rover's initial position
        // before doing the sweep.
        pivot_r(120)
    }
    
    maqueen.motorStopAll()
}

function forward () {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
}

function reverse () {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
}

function pivot_l (deg: number) {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
    basic.pause(deg * 5)
}

function pivot_r (deg: number) {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
    basic.pause(deg * 5)
}

basic.forever(function () {
    if (maqueen.sensor(PingUnit.Centimeters) != 0 && maqueen.sensor(PingUnit.Centimeters) <= 10) {
        maqueen.motorStopAll()
        basic.pause(250)
        
        reverse()
        basic.pause(250)
        
        sweep()
        basic.pause(250)
    } else {
        forward()
    }
})
