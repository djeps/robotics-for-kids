let d1 = 0
let d2 = 0

function forward () {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
}

function reverse () {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
}

function pivot_l (deg: number) {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
    basic.pause(deg * 5)
}

function pivot_r (deg: number) {
    maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
    maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
    basic.pause(deg * 5)
}

basic.forever(function () {
    if (maqueen.sensor(PingUnit.Centimeters) != 0 && maqueen.sensor(PingUnit.Centimeters) <= 10) {
        maqueen.motorStopAll()
        basic.pause(250)
        
        reverse()
        basic.pause(250)
        
        pivot_l(30)
        maqueen.motorStopAll()
        
        d1 = maqueen.sensor(PingUnit.Centimeters)
        
        pivot_r(60)
        maqueen.motorStopAll()
        
        d2 = maqueen.sensor(PingUnit.Centimeters)
        
        if (d2 < d1) {
            if (d1 >= 10) {
                pivot_l(60)
                maqueen.motorStopAll()
            } else {
                pivot_r(150)
                maqueen.motorStopAll()
            }
        }
    } else {
        forward()
    }
})
