let item = false

basic.forever(function () {
    if (maqueen.sensor(PingUnit.Centimeters) < 10 && maqueen.sensor(PingUnit.Centimeters) != 0) {
        item = Math.randomBoolean()
        
        if (item == true) {
            maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 125)
            maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 0)
            basic.pause(800)
        }
        
        if (item == false) {
            maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 0)
            maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 125)
            basic.pause(800)
        }
    } else {
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 75)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 75)
    }
})
