let d1 = 0
let d2 = 0

basic.forever(function () {
    if (maqueen.sensor(PingUnit.Centimeters) != 0 && maqueen.sensor(PingUnit.Centimeters) <= 7) {
        maqueen.motorStopAll()
        basic.pause(250)
        
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
        basic.pause(250)
        
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
        basic.pause(250)
        
        d1 = maqueen.sensor(PingUnit.Centimeters)
        
        maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CCW, 50)
        maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
        basic.pause(500)
        
        d2 = maqueen.sensor(PingUnit.Centimeters)
        
        if (d2 < d1) {
            maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
            maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CCW, 50)
            basic.pause(500)
        }
    } else {
        if (maqueen.sensor(PingUnit.Centimeters) > 7 && maqueen.sensor(PingUnit.Centimeters) <= 15) {
            maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 15)
            maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 15)
        } else {
            if (maqueen.sensor(PingUnit.Centimeters) > 15 && maqueen.sensor(PingUnit.Centimeters) <= 20) {
                maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 25)
                maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 25)
            } else {
                maqueen.MotorRun(maqueen.aMotors.M1, maqueen.Dir.CW, 50)
                maqueen.MotorRun(maqueen.aMotors.M2, maqueen.Dir.CW, 50)
            }
        }
    }
})
